﻿using System;

namespace ApproximateProbabilities
{
    class ApproximateProbabilities
    {
        public static void Main()
        {
            int sampleSize = 10000000;

            Console.Out.WriteLine("Calculating probabilites using Bayesian Networks.");
            Console.Out.WriteLine($"Each calculation is done with a sample size of {sampleSize}");
            Console.Out.WriteLine("*************************************************");
            Console.Out.WriteLine();

            var wetGrassDirectProb = WetGrassNetwork.WetGrassProbDirectSampling(sampleSize);

            Console.Out.WriteLine($"P(WetGrass) using Direct Sampling is {wetGrassDirectProb}");

            var wetGrassAndCloudyDirectProb = WetGrassNetwork.WetGrassAndCloudyProbDirectSampling(sampleSize);

            Console.Out.WriteLine($"P(WetGrass ^ Cloudy) using Direct Sampling is {wetGrassAndCloudyDirectProb}");

            Console.Out.WriteLine("*************************************************");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Now calculating P(WetGrass | Cloudy = true)");

            var wetGrassGivenCloudyRejection = WetGrassNetwork.WetGrassGivenCloudyRejectionSampling(sampleSize);
            Console.Out.WriteLine($"Rejection Sampling gives a probability of {wetGrassGivenCloudyRejection}");

            var wetGrassGivenCloudyWeighted = WetGrassNetwork.WetGrassGivenCloudyWeighted(sampleSize);
            Console.Out.WriteLine($"Limitation Weighting gives a probability of {wetGrassGivenCloudyWeighted}");

            Console.Out.WriteLine("*************************************************");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Now calculating P(Sprinkler|Rain=true)");

            var sprinklerOnGivenRainRejection = WetGrassNetwork.SprinklerOnGivenRainRejectionSampling(sampleSize);
            Console.Out.WriteLine($"Rejection Sampling gives a probability of {sprinklerOnGivenRainRejection}");

            var sprinklerOnGivenRainWeighted = WetGrassNetwork.SprinklerOnGivenRainWeighted(sampleSize);
            Console.Out.WriteLine($"Limitation Weighting gives a probability of {sprinklerOnGivenRainWeighted}");

            Console.Out.WriteLine("*************************************************");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Now calculating P(Earthquake|MaryCalls ^ ~JohnCalls)");

            var earthQuakeGivenMaryCallsJohnDoesntRejection =
                EarthquakeNetwork.EarthQuakeGivenMaryCallsJohnDoesntRejectionSampling(sampleSize);
            Console.Out.WriteLine($"Rejection Sampling gives a probability of {earthQuakeGivenMaryCallsJohnDoesntRejection}");

            var earthQuakeGivenMaryCallsJohnDoesntWeighting =
                EarthquakeNetwork.EarthQuakeGivenMaryCallsJohnDoesntWeightingSampling(sampleSize);
            Console.Out.WriteLine($"Limitation Weighting gives a probability of {earthQuakeGivenMaryCallsJohnDoesntWeighting}");

            Console.Out.WriteLine("*************************************************");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Now calculating P(JohnCalls|Earthquake ^ Burglary)");

            var johnCallsGivenEarthquakeAndBurglaryRejection =
        EarthquakeNetwork.JohnCallsGivenEarthquakeAndBurglaryRejectionSampling(sampleSize);
                Console.Out.WriteLine($"Rejection Sampling gives a probability of {johnCallsGivenEarthquakeAndBurglaryRejection}");

            var johnCallsGivenEarthquakeAndBurglaryWeighting =
                EarthquakeNetwork.JohnCallsGivenEarthquakeAndBurglaryWeightingSampling(sampleSize);
            Console.Out.WriteLine($"Limitation Weighting gives a probability of {johnCallsGivenEarthquakeAndBurglaryWeighting}");
        }
    }
}
