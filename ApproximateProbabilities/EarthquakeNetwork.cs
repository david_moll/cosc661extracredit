﻿using System.Collections.Generic;
using System.Linq;

namespace ApproximateProbabilities
{
    public struct EarthquakeWorld
    {
        public bool Burglary;
        public bool Earthquake;
        public bool Alarm;
        public bool JohnCalls;
        public bool MaryCalls;
        public double Weight;
    }

    class EarthquakeNetwork : BayesianNetwork
    {
        public static bool BurglaryProbability()
        {
            double burglaryProbability = 0.001;

            var burglaryHappened = GetWeightedProbability(burglaryProbability);

            return burglaryHappened;
        }

        public static bool EarthquakeProbability()
        {
            double earthquakeProbability = 0.002;
            var earthquakeHappened = GetWeightedProbability(earthquakeProbability);

            return earthquakeHappened;
        }

        public static bool JohnCalls(bool alarmRinging)
        {
            double johnCallsIfAlarm = 0.9;
            double johnCallsIfNoAlarm = 0.05;

            bool johnCalls = false;
            
            if (alarmRinging)
            {
                johnCalls = GetWeightedProbability(johnCallsIfAlarm);
            }
            else
            {
                johnCalls = GetWeightedProbability(johnCallsIfNoAlarm);
            }

            return johnCalls;
        }

        public static bool MaryCalls(bool alarmRinging)
        {
            double maryCallsIfAlarm = 0.7;
            double maryCallsIfNoAlarm = 0.01;

            bool maryCalls = false;
            
            if (alarmRinging)
            {
                maryCalls = GetWeightedProbability(maryCallsIfAlarm);
            }
            else
            {
                maryCalls = GetWeightedProbability(maryCallsIfNoAlarm);
            }

            return maryCalls;
        }

        public static bool AlarmGivenBurglaryAndEarthquake(bool burglary, bool earthquake)
        {
            double burglaryAndEarthquake = 0.95;
            double burglaryAndNoEarthquake = 0.94;
            double noBurglaryAndEarthquake = 0.29;
            double noBurglaryNoEarthquake = 0.001;

            bool alarm = false;

            if (burglary && earthquake)
            {
                alarm = GetWeightedProbability(burglaryAndEarthquake);
            }
            else if (burglary && !earthquake)
            {
                alarm = GetWeightedProbability(burglaryAndNoEarthquake);
            }
            else if (!burglary && earthquake)
            {
                alarm = GetWeightedProbability(noBurglaryAndEarthquake);
            }
            else // !burglary & !earthquake
            {
                alarm = GetWeightedProbability(noBurglaryNoEarthquake);
            }

            return alarm;
        }

        public static List<EarthquakeWorld> GetSampleWorlds(int sampleSize)
        {
            var rejectionSamplingWorlds = new List<EarthquakeWorld>(sampleSize);
            EarthquakeWorld testWorld = new EarthquakeWorld();

            for (int i = 0; i < sampleSize; i++)
            {
                testWorld = SampleWetEarthquakeWorld();
                rejectionSamplingWorlds.Add(testWorld);
            }

            return rejectionSamplingWorlds;
        }

        private static EarthquakeWorld SampleWetEarthquakeWorld()
        {
            EarthquakeWorld testValues;
            testValues.Weight = 1.0;

            testValues.Burglary = BurglaryProbability();
            testValues.Earthquake = EarthquakeProbability();
            testValues.Alarm = AlarmGivenBurglaryAndEarthquake(testValues.Burglary, testValues.Earthquake);
            testValues.JohnCalls = JohnCalls(testValues.Alarm);
            testValues.MaryCalls = MaryCalls(testValues.Alarm);

            return testValues;
        }

        public static EarthquakeWorld LikelihoodEarthquakeMaryCallsNotJohn()
        {
            // initialize the test vector
            EarthquakeWorld testValues;
            testValues.Weight = 1.0;

            // first sample our non-evidence variables
            testValues.Burglary = BurglaryProbability();
            testValues.Earthquake = EarthquakeProbability();
            testValues.Alarm = AlarmGivenBurglaryAndEarthquake(testValues.Burglary, testValues.Earthquake);

            // Mary calls is true, John calls is false
            testValues.MaryCalls = true;
            // so set the weight appropriately
            if (testValues.Alarm)
                testValues.Weight *= 0.70;
            else
                testValues.Weight *= 0.01;

            // now do the same for John
            testValues.JohnCalls = false;
            if (testValues.Alarm)
                testValues.Weight *= 0.10;
            else
                testValues.Weight *= 0.95;

            return testValues;
        }

        public static EarthquakeWorld LikelihoodJohnCallsEarthquakeAndBurglary()
        {
            // initialize the test vector
            EarthquakeWorld testValues;
            testValues.Weight = 1.0;

            // set our evidence variables and update the weight
            testValues.Burglary = true;
            testValues.Weight *= 0.001;
            testValues.Earthquake = true;
            testValues.Weight *= 0.002;

            // now get our non-evidence variables
            testValues.Alarm = AlarmGivenBurglaryAndEarthquake(testValues.Burglary, testValues.Earthquake);
            testValues.JohnCalls = JohnCalls(testValues.Alarm);
            testValues.MaryCalls = MaryCalls(testValues.Alarm);

            return testValues;
        }

        public static double EarthQuakeGivenMaryCallsJohnDoesntRejectionSampling(int sampleSize)
        {
            //P(Earthquake|MaryCalls ^ ~JohnCalls)
            // with Rejection Sampling

            // generate sample worlds
            var rejectionSamplingWorlds = EarthquakeNetwork.GetSampleWorlds(sampleSize);

            // get all the worlds where Mary calls and John does NOT
            var maryCallsJohnDoesntCallWorlds = rejectionSamplingWorlds.FindAll(x => x.MaryCalls && !x.JohnCalls);

            // now count the number of worlds from that subset for earthquakes
            double earthquakeWorlds = maryCallsJohnDoesntCallWorlds.Count(x => x.Earthquake);

            // now calculate the probability, and don't forget to normalize
            double earthQuakeMaryCallsNotJohn = earthquakeWorlds / (double)maryCallsJohnDoesntCallWorlds.Count;

            return earthQuakeMaryCallsNotJohn;
        }

        public static double EarthQuakeGivenMaryCallsJohnDoesntWeightingSampling(int sampleSize)
        {
            //P(Earthquake|MaryCalls ^ ~JohnCalls)
            // with Likelihood weighting

            var likelihoodWeightingWorlds = new List<EarthquakeWorld>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                likelihoodWeightingWorlds.Add(EarthquakeNetwork.LikelihoodEarthquakeMaryCallsNotJohn());
            }

            // we only want worlds where an earthquake happened
            var earthquakeWorlds = likelihoodWeightingWorlds.FindAll(x => x.Earthquake);

            // now sum up the weights
            double earthquakeWeightedSum = earthquakeWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            double normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double earthQuakeProbability = earthquakeWeightedSum / normalizationFactor;

            return earthQuakeProbability;
        }

        public static double JohnCallsGivenEarthquakeAndBurglaryRejectionSampling(int sampleSize)
        {
            //P(JohnCalls|Earthquake ^ Burglary)

            var rejectionSamplingWorlds = EarthquakeNetwork.GetSampleWorlds(sampleSize);

            // get all the worlds where there is an earthquake AND a burglary
            var earthquakeAndBurglaryWorlds = rejectionSamplingWorlds.FindAll(x => x.Earthquake && x.Burglary);

            //Console.Out.WriteLine($"{earthquakeAndBurglaryWorlds.Count} Earthquake and Burglary worlds.");

            // now count how many of those worlds have John call
            double johnCallsWorlds = earthquakeAndBurglaryWorlds.Count(x => x.JohnCalls);

            //Console.Out.WriteLine($"{johnCallsWorlds} times John Calls given Earthquake and Burglary.");

            // now calculate the probbaility, and don't forget to normalize
            double johnCallsBurglaryAndEarthquake = johnCallsWorlds / (double)earthquakeAndBurglaryWorlds.Count;

            return johnCallsBurglaryAndEarthquake;
        }

        public static double JohnCallsGivenEarthquakeAndBurglaryWeightingSampling(int sampleSize)
        {
            // P(JohnCalls|Earthquake ^ Burglary)

            var likelihoodWeightingWorlds = new List<EarthquakeWorld>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                likelihoodWeightingWorlds.Add(EarthquakeNetwork.LikelihoodJohnCallsEarthquakeAndBurglary());
            }

            // we only want worlds where John calls
            var johnCallsWorlds = likelihoodWeightingWorlds.FindAll(x => x.JohnCalls);

            // now sum up the weights
            double johnCallsWeightedSum = johnCallsWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            double normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double johnCallsProbability = johnCallsWeightedSum / normalizationFactor;

            return johnCallsProbability;
        }
    }
}
