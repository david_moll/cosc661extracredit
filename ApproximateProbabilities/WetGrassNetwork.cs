﻿using System.Collections.Generic;
using System.Linq;

namespace ApproximateProbabilities
{
    public struct GrassWorld
    {
        public bool isCloudy;
        public bool sprinklerOn;
        public bool isRaining;
        public bool WetGrass;
        public double Weight;

    }

    class WetGrassNetwork : BayesianNetwork
    {        
        public static bool IsCloudy()
        {
            double cloudyProbability = 0.5;
            bool isCloudy = false;

            isCloudy = GetWeightedProbability(cloudyProbability);

            return isCloudy;
        }

        internal static bool IsSprinklerOnGivenCloudy(bool cloudy)
        {
            double sprinklerOnIfCloudy = 0.1;
            double sprinklerOnIfNotCloudy = 0.5;

            bool sprinklerIsOn = false;

            // if it's cloudy, there's a 0.1 chance the sprinkler is on
            if (cloudy)
            {
                sprinklerIsOn = GetWeightedProbability(sprinklerOnIfCloudy);
            }
            else
            {
                sprinklerIsOn = GetWeightedProbability(sprinklerOnIfNotCloudy);
            }

            return sprinklerIsOn;
        }

        public static bool IsRainingGivenCloudy(bool isCloudy)
        {
            double rainingIfCloudy = 0.8;
            double rainingIfNotCloudy = 0.2;

            bool isRaining = false;

            // if it's cloudy, there's a 0.8 chance the sprinkler is on
            if (isCloudy)
            {
                isRaining = GetWeightedProbability(rainingIfCloudy);
            }
            else
            {
                isRaining = GetWeightedProbability(rainingIfNotCloudy);
            }

            return isRaining;
        }

        public static bool IsGrassWetGivenSprinklerAndRaining(bool sprinklerOn, bool raining)
        {
            double rainingAndSprinkler = 0.99;
            double notRainingAndSprinkler = 0.9;
            double rainingAndNotSprinkler = 0.9;
            double notRainingNotSprinkler = 0;

            bool wetGrass = false;

            if (sprinklerOn && raining)
            {
                wetGrass = GetWeightedProbability(rainingAndSprinkler);
            }
            else if (sprinklerOn && !raining)
            {
                wetGrass = GetWeightedProbability(notRainingAndSprinkler);
            }
            else if (!sprinklerOn && raining)
            {
                wetGrass = GetWeightedProbability(rainingAndNotSprinkler);
            }
            else // !sprinklerOn & !raining
            {
                wetGrass = GetWeightedProbability(notRainingNotSprinkler);
            }

            return wetGrass;
        }

        public static GrassWorld SampleWetGrassWorld()
        {
            GrassWorld testValues;
            testValues.Weight = 1.0;

            testValues.isCloudy = IsCloudy();
            testValues.sprinklerOn = IsSprinklerOnGivenCloudy(testValues.isCloudy);
            testValues.isRaining = IsRainingGivenCloudy(testValues.isCloudy);
            testValues.WetGrass =
                IsGrassWetGivenSprinklerAndRaining(testValues.sprinklerOn, testValues.isRaining);


            return testValues;
        }

        public static GrassWorld LikelihoodSampleWetGrassWorldCloudyTrue()
        {
            GrassWorld testValues;
            // set cloudy to true and set the weight to the chance it is cloudy
            testValues.isCloudy = true;
            testValues.Weight = 0.5;

            testValues.sprinklerOn = IsSprinklerOnGivenCloudy(testValues.isCloudy);
            testValues.isRaining = IsRainingGivenCloudy(testValues.isCloudy);
            testValues.WetGrass =
                IsGrassWetGivenSprinklerAndRaining(testValues.sprinklerOn, testValues.isRaining);
            
            return testValues;
        }

        public static GrassWorld LikelihoodSampleSprinklerWorldRainingTrue()
        {
            // initialize the test vector
            GrassWorld testValues;
            testValues.Weight = 1.0;

            testValues.isCloudy = IsCloudy();
            testValues.sprinklerOn = IsSprinklerOnGivenCloudy(testValues.isCloudy);
            testValues.isRaining = true;

            // set the weight according to the value for is raining
            // according to the probabilities for raining
            if (testValues.isCloudy)
                testValues.Weight *= 0.8;
            else
                testValues.Weight *= 0.2;

            testValues.WetGrass =
                IsGrassWetGivenSprinklerAndRaining(testValues.sprinklerOn, testValues.isRaining);

            return testValues;
        }

        internal static List<GrassWorld> GetSampleWorlds(int sampleSize)
        {
            var rejectionSamplingWorlds = new List<GrassWorld>(sampleSize);
            GrassWorld testWorld = new GrassWorld();

            for (int i = 0; i < sampleSize; i++)
            {
                testWorld = SampleWetGrassWorld();
                rejectionSamplingWorlds.Add(testWorld);
            }

            return rejectionSamplingWorlds;
        }

        public static double WetGrassProbDirectSampling(int sampleSize)
        {
            var directSamplingTest = GetSampleWorlds(sampleSize);

            // count how many "worlds" had wet grass
            var numWetGrass = directSamplingTest.Count(x => x.WetGrass == true);
            double probabilityWetGrass = numWetGrass / (double)sampleSize;

            return probabilityWetGrass;
        }

        public static double WetGrassAndCloudyProbDirectSampling(int sampleSize)
        {
            var directSamplingTest = GetSampleWorlds(sampleSize);

            // count how many "worlds" where it was cloudy and the grass was wet
            var numWetGrass = directSamplingTest.Count(x => (x.WetGrass == true && x.isCloudy == true));
            double probabilityWetGrassAndCloudy = numWetGrass / (double)sampleSize;

            return probabilityWetGrassAndCloudy;
        }

        public static double WetGrassGivenCloudyRejectionSampling(int sampleSize)
        {
            // generate sample worlds
            var rejectionSamplingWorlds = GetSampleWorlds(sampleSize);

            // get all the worlds where it is cloudy
            var cloudyWorlds = rejectionSamplingWorlds.FindAll(x => x.isCloudy);

            // count the number of worlds where the grass is wet
            var numWetGrass = cloudyWorlds.Count(x => x.WetGrass);

            // now calculate the probability, and don't forget to normalize
            double wetGrassGivenCloudy = (double)numWetGrass / (double)cloudyWorlds.Count;

            return wetGrassGivenCloudy;
        }

        public static double WetGrassGivenCloudyWeighted(int sampleSize)
        {
            // generate sample worlds
            var likelihoodWeightingWorlds = new List<GrassWorld>(sampleSize);
            GrassWorld testWorld = new GrassWorld();

            for (int i = 0; i < sampleSize; i++)
            {
                testWorld = WetGrassNetwork.LikelihoodSampleWetGrassWorldCloudyTrue();
                likelihoodWeightingWorlds.Add(testWorld);
            }

            // we only want the worlds where the grass is wet
            var wetGrassWorlds = likelihoodWeightingWorlds.FindAll(x => x.WetGrass);

            // now sum up the weights
            var wetGrassWeightedSum = wetGrassWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            var normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double wetGrassProbability = wetGrassWeightedSum / normalizationFactor;

            return wetGrassProbability;
        }

        public static double SprinklerOnGivenRainRejectionSampling(int sampleSize)
        {
            // generate sample worlds
            var rejectionSamplingWorlds = GetSampleWorlds(sampleSize);

            // get all the worlds where it is raining
            var rainingWorlds = rejectionSamplingWorlds.FindAll(x => x.isRaining);

            // determine the probability that the sprinkler is on
            var numSprinklerOn = rainingWorlds.Count(x => x.sprinklerOn);
           

            // now calculate the probability, and don't forget to normalize
            double sprinklerGivenRaining = (double)numSprinklerOn / (double)rainingWorlds.Count;

            return sprinklerGivenRaining;
        }

        public static double SprinklerOnGivenRainWeighted(int sampleSize)
        {
            // generate sample worlds
            var likelihoodWeightingWorlds = new List<GrassWorld>(sampleSize);
            GrassWorld testWorld = new GrassWorld();

            for (int i = 0; i < sampleSize; i++)
            {
                testWorld = WetGrassNetwork.LikelihoodSampleSprinklerWorldRainingTrue();
                likelihoodWeightingWorlds.Add(testWorld);
            }

            // we only want the worlds where the sprinkler is on
            var sprinklerOnWorlds = likelihoodWeightingWorlds.FindAll(x => x.sprinklerOn);

            // now sum up the weights 
            var sprinklerOnWeightedSum = sprinklerOnWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            var normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double wetGrassProbability = sprinklerOnWeightedSum / normalizationFactor;

            return wetGrassProbability;
        }
    }
}
