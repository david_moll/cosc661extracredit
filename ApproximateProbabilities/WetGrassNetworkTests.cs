﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace ApproximateProbabilities
{
    [TestFixture]
    class WetGrassNetworkTests
    {
        private const double TEST_DELTA = 0.05;
        int sampleSize = 10000000;

        [OneTimeSetUp]
        public void Init()
        {
            WetGrassNetwork.Rnd = new Random();
        }

        [Test]
        public void CloudyProbabilityTest()
        {
            var cloudyChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                cloudyChances.Add(WetGrassNetwork.IsCloudy());
            }

            var numCloudy = cloudyChances.Count(x => x == true);

            double probabilityIsCloudy = numCloudy / (double)sampleSize;

            Assert.AreEqual(0.5, probabilityIsCloudy, TEST_DELTA);
        }

        [Test]
        public void SprinklerIfCloudyTest()
        {

            var sprinklerChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                sprinklerChances.Add(WetGrassNetwork.IsSprinklerOnGivenCloudy(true));
            }

            var numSprinkler = sprinklerChances.Count(x => x == true);

            double probabilityIsSprinkler = numSprinkler / (double)sampleSize;

            Assert.AreEqual(0.1, probabilityIsSprinkler, TEST_DELTA);
        }

        [Test]
        public void SprinklerIfNotCloudyTest()
        {
            var sprinklerChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                sprinklerChances.Add(WetGrassNetwork.IsSprinklerOnGivenCloudy(false));
            }

            var numSprinkler = sprinklerChances.Count(x => x == true);

            double probabilityIsSprinkler = numSprinkler / (double)sampleSize;

            Assert.AreEqual(0.5, probabilityIsSprinkler, TEST_DELTA);
        }

        [Test]
        public void RainIfCloudyTest()
        {
            var rainChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                rainChances.Add(WetGrassNetwork.IsRainingGivenCloudy(true));
            }

            var numRain = rainChances.Count(x => x == true);

            double probabilityIsRain = numRain / (double)sampleSize;

            Assert.AreEqual(0.8, probabilityIsRain, TEST_DELTA);
        }

        [Test]
        public void RainIfNotCloudyTest()
        {
            var rainChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                rainChances.Add(WetGrassNetwork.IsRainingGivenCloudy(false));
            }

            var numRain = rainChances.Count(x => x == true);

            double probabilityIsRain = numRain / (double)sampleSize;

            Assert.AreEqual(0.2, probabilityIsRain, TEST_DELTA);
        }

        [Test]
        public void WetGrassTest()
        {
            var wetGrassSprinklingRaining = new List<bool>(sampleSize);
            var wetGrassSprinklingNotRaining = new List<bool>(sampleSize);
            var wetGrassNotSprinklingRaining = new List<bool>(sampleSize);
            var wetGrassNeither = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                wetGrassSprinklingRaining.Add(WetGrassNetwork.IsGrassWetGivenSprinklerAndRaining(true, true));
                wetGrassSprinklingNotRaining.Add(WetGrassNetwork.IsGrassWetGivenSprinklerAndRaining(true, false));
                wetGrassNotSprinklingRaining.Add(WetGrassNetwork.IsGrassWetGivenSprinklerAndRaining(false, true));
                wetGrassNeither.Add(WetGrassNetwork.IsGrassWetGivenSprinklerAndRaining(false, false));
            }

            var numWetGrassSandR = wetGrassSprinklingRaining.Count(x => x == true);
            var numWetGrassSnotR = wetGrassSprinklingNotRaining.Count(x => x == true);
            var numWetGrassNotSandR = wetGrassNotSprinklingRaining.Count(x => x == true);
            var numWetGrassNeither = wetGrassNeither.Count(x => x == true);

            double probWGifSandR = numWetGrassSandR / (double)sampleSize;
            double probWGifSnotR = numWetGrassSnotR / (double)sampleSize;
            double probWGifNotSandR = numWetGrassNotSandR / (double)sampleSize;
            double probWGifNeither = numWetGrassNeither / (double)sampleSize;

            Assert.AreEqual(0.99, probWGifSandR, TEST_DELTA);
            Assert.AreEqual(0.9, probWGifSnotR, TEST_DELTA);
            Assert.AreEqual(0.9, probWGifNotSandR, TEST_DELTA);
            Assert.AreEqual(0.0, probWGifNeither, TEST_DELTA);
        }

        [Test]
        public void WetGrassProbabilityDirectSamplingTest()
        {
            var directSamplingTest = WetGrassNetwork.GetSampleWorlds(sampleSize);

            // count how many "worlds" had wet grass
            var numWetGrass = directSamplingTest.Count(x => x.WetGrass == true);
            double probabilityWetGrass = numWetGrass / (double)sampleSize;

            Assert.AreEqual(0.64, probabilityWetGrass, TEST_DELTA);
            Console.Out.WriteLine($"Wet Grass Probability is {probabilityWetGrass}");
        }

        [Test]
        public void WetGrassAndCloudyDirectSamplingTest()
        {
            var directSamplingTest = WetGrassNetwork.GetSampleWorlds(sampleSize);

            // count how many "worlds" where it was cloudy and the grass was wet
            var numWetGrass = directSamplingTest.Count(x => (x.WetGrass == true && x.isCloudy == true));
            double probabilityWetGrassAndCloudy = numWetGrass / (double)sampleSize;

            Assert.AreEqual(0.37, probabilityWetGrassAndCloudy, TEST_DELTA);
            Console.Out.WriteLine($"Wet Grass AND Cloudy Probability is {probabilityWetGrassAndCloudy}");
        }

        [Test]
        public void SprinklerGivenRainingRejectionSamplingTest()
        {
            //P(Sprinkler|Rain=true)

            // generate sample worlds
            var rejectionSamplingWorlds = WetGrassNetwork.GetSampleWorlds(sampleSize);

            // get all the worlds where it is raining
            var rainingWorlds = rejectionSamplingWorlds.FindAll(x => x.isRaining);

            // determine the probability that the sprinkler is on
            var numSprinklerOn = rainingWorlds.Count(x => x.sprinklerOn);
            var numSprinklerOff = rainingWorlds.Count(x => !x.sprinklerOn);

            // double check our data makes sense
            Assert.AreEqual(rainingWorlds.Count, numSprinklerOff + numSprinklerOn);

            // now calculate the probability, and don't forget to normalize
            double sprinklerGivenRaining = (double) numSprinklerOn / (double) rainingWorlds.Count;

            Assert.AreEqual(0.18, sprinklerGivenRaining, TEST_DELTA);
        }

        [Test]
        public void SprinklerGivenRainingLikelihoodWeightingTest()
        {
            // generate sample worlds
            var likelihoodWeightingWorlds = new List<GrassWorld>(sampleSize);
            GrassWorld testWorld = new GrassWorld();

            for (int i = 0; i < sampleSize; i++)
            {
                testWorld = WetGrassNetwork.LikelihoodSampleSprinklerWorldRainingTrue();
                likelihoodWeightingWorlds.Add(testWorld);
            }

            // we only want the worlds where the sprinkler is on
            var sprinklerOnWorlds = likelihoodWeightingWorlds.FindAll(x => x.sprinklerOn);

            // now sum up the weights 
            var sprinklerOnWeightedSum = sprinklerOnWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            var normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double wetGrassProbability = sprinklerOnWeightedSum / normalizationFactor;

            Assert.AreEqual(0.18, wetGrassProbability, TEST_DELTA);
        }

        [Test]
        public void WetGrassGivenCloudyRejectionSamplingTest()
        {
            // generate sample worlds
            var rejectionSamplingWorlds = WetGrassNetwork.GetSampleWorlds(sampleSize);

            // get all the worlds where it is cloudy
            var cloudyWorlds = rejectionSamplingWorlds.FindAll(x => x.isCloudy);

            // count the number of worlds where the grass is wet
            var numWetGrass = cloudyWorlds.Count(x => x.WetGrass);

            // now calculate the probability, and don't forget to normalize
            double wetGrassGivenCloudy = (double) numWetGrass / (double)cloudyWorlds.Count;

            Assert.AreEqual(0.74, wetGrassGivenCloudy, TEST_DELTA);

        }

        [Test]
        public void WetGrassGivenCloudyLikelihoodWeightingTest()
        {
            // generate sample worlds
            var likelihoodWeightingWorlds = new List<GrassWorld>(sampleSize);
            GrassWorld testWorld = new GrassWorld();

            for (int i = 0; i < sampleSize; i++)
            {
                testWorld = WetGrassNetwork.LikelihoodSampleWetGrassWorldCloudyTrue();
                likelihoodWeightingWorlds.Add(testWorld);
            }

            // we only want the worlds where the grass is wet
            var wetGrassWorlds = likelihoodWeightingWorlds.FindAll(x => x.WetGrass);

            // now sum up the weights
            var wetGrassWeightedSum = wetGrassWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            var normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double wetGrassProbability = wetGrassWeightedSum / normalizationFactor;

            Assert.AreEqual(0.74, wetGrassProbability, TEST_DELTA);
        }
    }
}
