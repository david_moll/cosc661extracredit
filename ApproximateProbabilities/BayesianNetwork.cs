﻿using System;

namespace ApproximateProbabilities
{
    class BayesianNetwork
    {
        // our random variable
        private static Random rnd;

        // Set up the random variable as a singleton
        internal static Random Rnd
        {
            get
            {
                if (rnd == null)
                    rnd = new Random();
                return rnd;
            }
            set { rnd = value; }
        }

        /// <summary>
        /// Return true or false for a random variable
        /// with the specified weight.  
        /// </summary>
        /// <param name="weight">The weight of the random variable</param>
        /// <returns>True weight% of the time</returns>
        internal static bool GetWeightedProbability(double weight)
        {
            // get a random number between 0.0 and < 1.0
            var randValue = Rnd.NextDouble();

            return randValue <= weight;
        }

    }
}
