﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace ApproximateProbabilities
{
    [TestFixture]
    class EarthquakeNetworkTests
    {
        private const double TEST_DELTA = 0.05;
        private const double SMALL_TEST_DELTA = 0.001;
        int sampleSize = 10000000;
        

        [OneTimeSetUp]
        public void Init()
        {
            BayesianNetwork.Rnd = new Random();
        }

        [Test]
        public void BurglaryProbabilityTest()
        {
            var burglaryChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                burglaryChances.Add(EarthquakeNetwork.BurglaryProbability());
            }

            double numBurglary = burglaryChances.Count(x => x);

            double probabilityBurglary = numBurglary / (double) sampleSize;

            Assert.AreEqual(.001, probabilityBurglary, SMALL_TEST_DELTA);
        }

        [Test]
        public void EarthquakeProbabilityTest()
        {
            var earthquakeChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                earthquakeChances.Add(EarthquakeNetwork.EarthquakeProbability());
            }

            double numEarthquake = earthquakeChances.Count(x => x);

            double probabilityEarthquake = numEarthquake / (double)sampleSize;

            Assert.AreEqual(.002, probabilityEarthquake, SMALL_TEST_DELTA);
        }

        [Test]
        public void JohnCallsGivenAlarmTest()
        {
            var johnCallsChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                johnCallsChances.Add(EarthquakeNetwork.JohnCalls(true));
            }

            double numJohnCalls = johnCallsChances.Count(x => x);
            double probabilityJohnCalls = numJohnCalls / (double) sampleSize;

            Assert.AreEqual(0.90, probabilityJohnCalls, TEST_DELTA);
        }

        [Test]
        public void JohnCallsNoAlarmTest()
        {
            var johnCallsChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                johnCallsChances.Add(EarthquakeNetwork.JohnCalls(false));
            }

            double numJohnCalls = johnCallsChances.Count(x => x);
            double probabilityJohnCalls = numJohnCalls / (double)sampleSize;

            Assert.AreEqual(0.05, probabilityJohnCalls, SMALL_TEST_DELTA);
        }

        [Test]
        public void MaryCallsAlarmTest()
        {
            var maryCallsChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                maryCallsChances.Add(EarthquakeNetwork.MaryCalls(true));
            }

            double numMaryCalls = maryCallsChances.Count(x => x);
            double probabilityMaryCalls = numMaryCalls / (double) sampleSize;

            Assert.AreEqual(0.7, probabilityMaryCalls, TEST_DELTA);
        }

        [Test]
        public void MaryCallsNoAlarmTest()
        {
            var maryCallsChances = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                maryCallsChances.Add(EarthquakeNetwork.MaryCalls(false));
            }

            double numMaryCalls = maryCallsChances.Count(x => x);
            double probabilityMaryCalls = numMaryCalls / (double)sampleSize;

            Assert.AreEqual(0.01, probabilityMaryCalls, SMALL_TEST_DELTA);
        }

        [Test]
        public void AlarmTest()
        {
            var alarmBurglaryAndEarthquake = new List<bool>(sampleSize);
            var alarmBurglaryAndNoEarthquake = new List<bool>(sampleSize);
            var alarmNoBurglaryAndEarthquake = new List<bool>(sampleSize);            
            var alarmNeither = new List<bool>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                alarmBurglaryAndEarthquake.Add(EarthquakeNetwork.AlarmGivenBurglaryAndEarthquake(true, true));
                alarmBurglaryAndNoEarthquake.Add(EarthquakeNetwork.AlarmGivenBurglaryAndEarthquake(true, false));
                alarmNoBurglaryAndEarthquake.Add(EarthquakeNetwork.AlarmGivenBurglaryAndEarthquake(false, true));
                alarmNeither.Add(EarthquakeNetwork.AlarmGivenBurglaryAndEarthquake(false, false));
            }

            var numAlarmBandE = alarmBurglaryAndEarthquake.Count(x => x);
            var numAlarmBandNoE = alarmBurglaryAndNoEarthquake.Count(x => x);
            var numAlarmNoBandE = alarmNoBurglaryAndEarthquake.Count(x => x);
            var numAlarmNeither = alarmNeither.Count(x => x);

            double probAlarmBandE   = numAlarmBandE   / (double) sampleSize;
            double probAlarmBandNoE = numAlarmBandNoE / (double) sampleSize;
            double probAlarmNoBandE = numAlarmNoBandE / (double) sampleSize;
            double probAlarmNeither = numAlarmNeither / (double)sampleSize;

            Assert.AreEqual(0.95, probAlarmBandE, TEST_DELTA);
            Assert.AreEqual(0.94, probAlarmBandNoE, TEST_DELTA);
            Assert.AreEqual(0.29, probAlarmNoBandE, TEST_DELTA);
            Assert.AreEqual(0.001, probAlarmNeither, SMALL_TEST_DELTA);
        }

        [Test]        
        public void EarthQuakeMaryCallsJohnDoesntCallRejectionTest()
        {
            //P(Earthquake|MaryCalls ^ ~JohnCalls)
            // with Rejection Sampling

            // generate sample worlds
            var rejectionSamplingWorlds = EarthquakeNetwork.GetSampleWorlds(sampleSize);

            // get all the worlds where Mary calls and John does NOT
            var maryCallsJohnDoesntCallWorlds = rejectionSamplingWorlds.FindAll(x => x.MaryCalls && !x.JohnCalls);

            // now count the number of worlds from that subset for earthquakes
            double earthquakeWorlds = maryCallsJohnDoesntCallWorlds.Count(x => x.Earthquake);

            // now calculate the probability, and don't forget to normalize
            double earthQuakeMaryCallsNotJohn = earthquakeWorlds / (double) maryCallsJohnDoesntCallWorlds.Count;

            Assert.AreEqual(0.0056, earthQuakeMaryCallsNotJohn, SMALL_TEST_DELTA);
        }

        [Test]
        public void EarthQuakeMaryCallsJohnDoesntCallWeightingTest()
        {
            //P(Earthquake|MaryCalls ^ ~JohnCalls)
            // with Likelihood weighting

            var likelihoodWeightingWorlds = new List<EarthquakeWorld>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                likelihoodWeightingWorlds.Add(EarthquakeNetwork.LikelihoodEarthquakeMaryCallsNotJohn());
            }

            // we only want worlds where an earthquake happened
            var earthquakeWorlds = likelihoodWeightingWorlds.FindAll(x => x.Earthquake);

            // now sum up the weights
            double earthquakeWeightedSum = earthquakeWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            double normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double earthQuakeProbability = earthquakeWeightedSum / normalizationFactor;

            Assert.AreEqual(0.0056, earthQuakeProbability, SMALL_TEST_DELTA);
        }

        [Test]
        public void JohnCallsEarthquakeAndBurglaryRejectionTest()
        {
            //P(JohnCalls|Earthquake ^ Burglary)

            var rejectionSamplingWorlds = EarthquakeNetwork.GetSampleWorlds(sampleSize);

            // get all the worlds where there is an earthquake AND a burglary
            var earthquakeAndBurglaryWorlds = rejectionSamplingWorlds.FindAll(x => x.Earthquake && x.Burglary);

            // now count how many of those worlds have John call
            double johnCallsWorlds = earthquakeAndBurglaryWorlds.Count(x => x.JohnCalls);

            // now calculate the probbaility, and don't forget to normalize
            double johnCallsBurglaryAndEarthquake = johnCallsWorlds / (double) earthquakeAndBurglaryWorlds.Count;

            Assert.AreEqual(0.9, johnCallsBurglaryAndEarthquake, TEST_DELTA);
        }

        [Test]
        public void JohnCallsEarthquakeAndBurglaryLikelihoodWeightingTest()
        {
            // P(JohnCalls|Earthquake ^ Burglary)

            var likelihoodWeightingWorlds = new List<EarthquakeWorld>(sampleSize);

            for (int i = 0; i < sampleSize; i++)
            {
                likelihoodWeightingWorlds.Add(EarthquakeNetwork.LikelihoodJohnCallsEarthquakeAndBurglary());
            }

            // we only want worlds where John calls
            var johnCallsWorlds = likelihoodWeightingWorlds.FindAll(x => x.JohnCalls);

            // now sum up the weights
            double johnCallsWeightedSum = johnCallsWorlds.Sum(x => x.Weight);

            // the normalization factor is the sum of the weights for all the worlds
            double normalizationFactor = likelihoodWeightingWorlds.Sum(x => x.Weight);

            // now calculate the probability
            double johnCallsProbability = johnCallsWeightedSum / normalizationFactor;

            Assert.AreEqual(0.9, johnCallsProbability, TEST_DELTA);
        }
    }
}
